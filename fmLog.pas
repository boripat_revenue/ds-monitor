unit fmLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, RXClock;

type
  TfrmLog = class(TForm)
    Memo1: TMemo;
    btnClear: TButton;
    btnMinimize: TButton;
    lblVersion: TLabel;
    txtMac: TEdit;
    RxClock1: TRxClock;
    txtSerial: TEdit;
    btnReload: TButton;
    btnClose: TButton;
    timExpire: TTimer;
    procedure btnClearClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnMinimizeClick(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnReloadClick(Sender: TObject);
    procedure timExpireTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLog: TfrmLog;


implementation
uses fmMain;
{$R *.dfm}

procedure TfrmLog.btnClearClick(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfrmLog.FormCreate(Sender: TObject);
begin
//  lblVersion.Caption:='Version '+cValMonitor;
  Memo1.Clear;
end;

procedure TfrmLog.btnMinimizeClick(Sender: TObject);
begin
  frmLog.Hide;
end;

procedure TfrmLog.Memo1Change(Sender: TObject);
begin
  if (Memo1.Lines.Count > 100) then Memo1.Clear;
end;

procedure TfrmLog.btnCloseClick(Sender: TObject);
begin
  frmMain.btnExit.Click;
end;

procedure TfrmLog.btnReloadClick(Sender: TObject);
var iSelButton:integer;
begin
  frmMain.timMonitor.Enabled:=false;
  frmMain.TimState.Enabled:=false;
  frmMain.killAgent;
  frmMain.killPlayer;
  Application.ProcessMessages;
  sleep(500);
{
  if (frmMain.CheckExpire) then begin
    iSelButton:=MessageDlg(cMsgExpire,mtInformation,[mbOK],0);
    frmMain.GetExpire();
    Application.Terminate;
    abort;
  end;
  frmMain.StartProgram;
}
//  frmMain.ExpireCheck;
  frmMain.SaveTrack('');
  frmMain.SaveTrack('Manual Reload');
  frmMain.StartProgram();

end;


procedure TfrmLog.timExpireTimer(Sender: TObject);
begin
  timExpire.Enabled:=false;
//  frmMain.ExpireCheck();
  frmMain.SaveTrack('');
  frmMain.SaveTrack('Program Start');
  frmMain.StartProgram();

end;

procedure TfrmLog.FormShow(Sender: TObject);
begin
  self.left:=screen.WorkAreaWidth-self.Width;
  self.Top:=screen.WorkAreaTop-self.Height;
end;

end.
