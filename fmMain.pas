unit fmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TlHelp32,INIFiles,DateUtils,StrUtils,ShellApi,
  RXShell, Menus, HotKeyManager,NB30, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP;

type
  TfrmMain = class(TForm)
    txtProcess: TEdit;
    btnFind: TButton;
    btnKill: TButton;
    timMonitor: TTimer;
    TimState: TTimer;
    chkMon: TCheckBox;
    chkState: TCheckBox;
    RxTrayIcon1: TRxTrayIcon;
    KeyHook: THotKeyManager;
    popMenu: TPopupMenu;
    mnuExit: TMenuItem;
    btnHide: TButton;
    btnShow: TButton;
    btnExit: TButton;
    txtMac: TEdit;
    IdHTTP1: TIdHTTP;
    Memo1: TMemo;
    txtSerial: TEdit;
    txtVerPlayer: TEdit;
    txtVerAgent: TEdit;
    mmoFiles: TMemo;
    procedure timStateTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure timMonitorTimer(Sender: TObject);
    procedure txtProcessChange(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnKillClick(Sender: TObject);
    procedure chkMonClick(Sender: TObject);
    procedure chkStateClick(Sender: TObject);
    procedure KeyHookHotKeyPressed(HotKey: Cardinal; Index: Word);
    procedure btnShowClick(Sender: TObject);
    procedure btnHideClick(Sender: TObject);
    procedure RxTrayIcon1DblClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure mnuExitClick(Sender: TObject);
    procedure mnuShowClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnuHideClick(Sender: TObject);
    procedure RxTrayIcon1Click(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    function GetAdapterInfo(Lana: Char): String;
    function GetMACAddress: String;
  public
    { Public declarations }
    procedure setDefault();
    procedure checkAgent();
    procedure checkPlayer();
    procedure killPlayer(bLog:boolean=FALSE);
    procedure killAgent(bLog:boolean=FALSE);
    procedure startPlayer(bLog:boolean=FALSE);
    procedure startAgent(bLog:boolean=FALSE);
    procedure SaveLog(sCategory: string);
    procedure SaveTrack(sValue:string);
    function checkAlive(sType:string; sRecord: string):boolean;
    procedure Alert2Server(sType:string;sDevice:string);
    function GenHTTPRequest(sURL:string; sCmd: string; sDevice: string): string;
    procedure StartProgram();
    function CheckExpire():boolean;
    procedure GetExpire();
    procedure ExpireCheck();
    procedure CreateDefaultFolder();
    procedure RemoveExpiredFile(sFolder:string;iDelMaxDay:integer=30);

  end;

const
  cProduct='InfoExpress-Lite';
  cKeyComputerID='ComputerID';
  cKeyMonitor='Monitor';
  cKeyPlayer='Player';
  cKeyAgent='Agent';
  cValMonitor='1.0.9';
  cKeyActivate='Activate';
  cKeyExpire='Expire';
  cMsgExpire='Application Expired.';
var
  sFileList:TStrings;
  bInprocess: boolean;
  frmMain: TfrmMain;
  sProgramDir: String;
  sPlayerName: string;
  sAgentName: String;
  iOnlineInterval: integer;
  iMonInterval: integer;
  iOfflineInterval: integer;
  iDiffGreen: integer;
  iDiffOrange: integer;

  MemoryStream: TMemoryStream;
  ResponseStr: TStringList;

  bTrack: boolean;
  bDebug: boolean;
  bExpire:boolean;
  sTrackLog: string;
  sOnlineMONLog: string;
  sOnlineSMPLog: string;
  sOnlineAGNLog: string;
  sClientID: string;
  sProductSMP: string;
  sProductAGN: string;
  sProductMON: string;
  iDelMaxDay: integer; // Keep Delete file if more than max day

  urlMonitor: string;
  urlUpdate: string;
  urlRegister: string;

  sProduct:string;
  sComputerID: string;
  sRegComputerID: string;
  sRegName01,sRegName02,sRegName03,sRegName04: string;
  pData01,pData02: pChar;
  sShow,sInputKey: string;
  sData01,sData02: string;
  sTmp01,sTmp02: string;

  sValPlayer,sValAgent:string;

  function FileInUse(FileName: string): Boolean;
  function processExists(exeFileName: string): Boolean;
  function KillTask(ExeFileName: string): Integer;
  function KillWindows(const sCapt: PChar) : boolean;

  procedure Split (const Delimiter: Char; Input: string;
   const Strings: TStrings) ;

  procedure dll_GetSerialize(out strOut: pChar); external 'RevenueExpress.dll';
  procedure dll_GenMD5(const strHash: pChar; Out strOut: pChar);  external 'RevenueExpress.dll';
  procedure dll_GetRegistry(const strPName: PChar; const strKey: PChar;Out strOut: PChar); external 'RevenueExpress.dll';
  function dll_AddRegistry(const strPName: PChar;const strKey: pChar; const strValue: pChar): boolean; external 'RevenueExpress.dll';
  procedure FreePChar(p: Pchar); external 'RevenueExpress.dll';

implementation

{$R *.dfm}
uses fmLog;

procedure ListFileDir(Path: string; FileList: TStrings);
var
  SR: TSearchRec;
begin
  if FindFirst(Path + '*.*', faAnyFile, SR) = 0 then
  begin
    repeat
      if (SR.Attr <> faDirectory) then
      begin
        FileList.Add(SR.Name);
      end;
    until FindNext(SR) <> 0;
    FindClose(SR);
  end;
end;

function FileTimeToDTime(FTime: TFileTime): TDateTime;
var
  LocalFTime: TFileTime;
  STime: TSystemTime;
begin
  FileTimeToLocalFileTime(FTime, LocalFTime);
  FileTimeToSystemTime(LocalFTime, STime);
  Result := SystemTimeToDateTime(STime);
end;

function GetFileDate(sFileName,sType:string;sFormat: string='yyyy-mm-dd hh:nn:ss'):string;
var
  dtCurrent: TDateTime;
  dtReturn: TDateTime;
  Day,Time: TStringList;
  Input: TStringList;
  myLogFile:TextFile;
  iDiff:integer;
  sFile,sValue,sNow : string;
  SR: TSearchRec;
begin
  result:= formatdatetime(sFormat,now);
//  sFile:=sProgramDir+sRecord;
{
  while (fileinuse(sFile)) do sleep(500);
  if (fileexists(SFile)) then
    begin
    AssignFile(myLogFile,sFile);
    Reset(myLogFile);
    ReadLn(myLogFile, sValue);
    CloseFile(myLogFile);
    end;
}
  Input:=TStringList.Create;
  Day:=TStringList.Create;
  Time:=TStringList.Create;
  if FindFirst(sFileName, faAnyFile, SR) = 0 then begin
    if (lowercase(sType) = 'create') then begin
      dtReturn := FileTimeToDTime(SR.FindData.ftCreationTime);
    end else if (lowercase(sType) = 'modify') then begin
      dtReturn := FileTimeToDTime(SR.FindData.ftLastWriteTime);
    end else begin
      dtReturn := FileTimeToDTime(SR.FindData.ftLastAccessTime);
//    dtLast := FileTimeToDTime(SR.FindData.ftLastAccessTime);
    end;
  end
  Else begin
    sValue:=formatdatetime('yyyy-mm-dd hh:nn:ss',Now());
    Split(' ',sValue,Input);
    Split('-',Input[0],Day);
    Split(':',Input[1],Time);
    dtReturn:=EncodeDateTime(strtoInt(Day[0]),strtoint(Day[1]),strtoint(Day[2]),
      strtoint(Time[0]),strtoint(Time[1]),strtoint(Time[2]),0);
  end;

  result:= formatdatetime(sFormat,dtReturn);
//  result:= formatdatetime('dd/mm/yyyy hh:nn:ss',dtReturn);
  Day.Free;
  Time.Free;
  Input.Free;
end;



function TfrmMain.GetAdapterInfo(Lana: Char): String;
var
  Adapter: TAdapterStatus;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBRESET);
  NCB.ncb_lana_num := Lana;
  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBASTAT);
  NCB.ncb_lana_num := Lana;
  NCB.ncb_callname := '*';

  FillChar(Adapter, SizeOf(Adapter), 0);
  NCB.ncb_buffer := @Adapter;
  NCB.ncb_length := SizeOf(Adapter);

  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  Result :=
    IntToHex(Byte(Adapter.adapter_address[0]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[1]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[2]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[3]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[4]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[5]), 2);
end;

function TfrmMain.GetMACAddress: string;
var
  AdapterList: TLanaEnum;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBENUM);
  NCB.ncb_buffer := @AdapterList;
  NCB.ncb_length := SizeOf(AdapterList);
  Netbios(@NCB);

  if Byte(AdapterList.length) > 0 then
    Result := GetAdapterInfo(AdapterList.lana[0])
  else
    Result := 'No MAC';
end;

procedure Split(const Delimiter: Char;Input: string;
    const Strings: TStrings) ;
begin
   Assert(Assigned(Strings)) ;
   Strings.Clear;
   Strings.Delimiter := Delimiter;
   Strings.DelimitedText := Input;
end;

procedure TfrmMain.SaveLog(sCategory: string);
var
  myLogFile:TextFile;
  sName,sDir,sFile,sNew,sValue : string;
  iRun:integer;
begin
  sFile:=sProgramDir+'log/online_mon.log';
  if (not FileExists(sFile)) then begin
    try
      AssignFile(myLogFile,sFile);
      rewrite(myLogFile);
      CloseFile(myLogFile);
    except
      on E : Exception do begin
        sValue:='Exception on Function = SaveLog('+sCategory+') #1';
        sValue:=sValue+#13#10+'Exception Class = '+E.ClassName;
        sValue:=sValue+#13#10+'Exception Message = '+E.Message;
        ShowMessage(sValue);
      end;
    end;
  end;
  sValue:=formatdatetime('yyyy-mm-dd hh:nn:ss',now);
  try
    iRun:=0;
    while ((FileInUse(sFile)) and (iRun < 100)) do begin
      sleep(random(100)+100);
      inc(iRun);
    end;
    if (not FileInUse(sFile)) then begin
      try
        AssignFile(myLogFile,sFile);
        Append(myLogFile);
        WriteLn(myLogFile, sValue);
        CloseFile(myLogFile);
      except
        on E : Exception do begin
          sValue:='Exception on Function = SaveLog('+sCategory+') #2';
          sValue:=sValue+#13#10+'Exception Class = '+E.ClassName;
          sValue:=sValue+#13#10+'Exception Message = '+E.Message;
          ShowMessage(sValue);
        end;
      end;
    end;
  Except
    on E : Exception do begin
      sValue:='Exception on Function = SaveLog('+sCategory+') #3';
      sValue:=sValue+#13#10+'Exception Class = '+E.ClassName;
      sValue:=sValue+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sValue);
    end;
  End;
  frmLog.memo1.Lines.Add(sValue+'# Online record');
end;

procedure TfrmMain.SaveTrack(sValue:string);
var
  myLogFile:TextFile;
  sFile : string;
  sDate,sInput: string;
  iRun: integer;
begin
  sDate:=formatdatetime('yyyymmdd',now);
  sFile:=sProgramDir+'log/'+sTrackLog+'_'+sDate+'.log';
  if (bTrack) then begin
    try
      if (not fileexists(sFile)) then begin
        try
          AssignFile(myLogFile,sFile);
          ReWrite(myLogFile);
          CloseFile(myLogFile);
        except
          on E : Exception do begin
            sInput:='Exception on Function = SaveTrack('+sValue+') #1';
            sInput:=sInput+#13#10+'Exception Class = '+E.ClassName;
            sInput:=sInput+#13#10+'Exception Message = '+E.Message;
            ShowMessage(sInput);
          end;
        end;
      end;
      iRun:=0;
      while ((FileInUse(sFile)) and (iRun < 100)) do begin
        sleep(random(100)+100);
        inc(iRun);
      end;
      if (not FileInUse(sFile)) then begin
        try
          sInput:=formatdatetime('yyyy/mm/dd hh:nn:ss #',now);
          AssignFile(myLogFile,sFile);
          Append(myLogFile);
          WriteLn(myLogFile, formatdatetime('yyyy/mm/dd hh:nn:ss',now)+' # '+sValue);
          CloseFile(myLogFile);
        except
          on E : Exception do begin
            sInput:='Exception on Function = SaveTrack('+sValue+') #2';
            sInput:=sInput+#13#10+'Exception Class = '+E.ClassName;
            sInput:=sInput+#13#10+'Exception Message = '+E.Message;
            ShowMessage(sInput);
          end;
        end;
      end;
    Except
      on E : Exception do begin
        sInput:='Exception on Function = SaveTrack('+sValue+') #3';
        sInput:=sInput+#13#10+'Exception Class = '+E.ClassName;
        sInput:=sInput+#13#10+'Exception Message = '+E.Message;
        ShowMessage(sInput);
      end;
    End;
    end;
end;

procedure TfrmMain.timStateTimer(Sender: TObject);
begin
  timState.Enabled:=false;
  SaveLog('online_mon');
  timState.Enabled:=true;
end;

procedure TfrmMain.setDefault();
var
  iniFile: TIniFile;
  sLogFile,sStarter,sMove: string;
begin

  sProgramDir:=ExtractFilePath(Application.ExeName);

  sProduct:='InfoExpress-Lite';
  sRegName01:='ComputerID';

  dll_getRegistry(cProduct,cKeyComputerID,pData01);
  sTmp01:=pData01;
  FreePChar(pData01);


  if (length(sTmp01) > 10) then
    begin
    sComputerID:=sTmp01;
    end
  Else
    begin
    dll_getSerialize(pData01);
    sTmp01:=pData01;
    FreePChar(pData01);

    dll_genMD5(pchar(sTmp01),pData01);
    sComputerID:=pData01;
    FreePChar(pData01);
    dll_addRegistry(cProduct,cKeyComputerID,pchar(sComputerID));
    end;
  dll_addRegistry(cProduct,cKeyMonitor,cValMonitor);

  txtSerial.Text:=sComputerID;

  dll_getRegistry(cProduct,cKeyPlayer,pData01);
  sValPlayer:=pData01;
  FreePChar(pData01);

  dll_getRegistry(cProduct,cKeyAgent,pData01);
  sValAgent:=pData01;
  FreePChar(pData01);

  txtVerPlayer.Text:=sValPlayer;
  txtVerAgent.Text:=sValAgent;

  sStarter:=sProgramDir+'tmp\dsstarter.exe';


  bInprocess:=false;
  bExpire:=true;
  frmLog.txtMac.Text:=GetMACAddress;
//  frmLog.txtSerial.Text:=txtSerial.Text;
  frmLog.txtSerial.Text:=AnsiLeftStr(txtSerial.Text,4)
    + '-'+ AnsiRightStr(txtSerial.Text,4);
  bTrack:=false;
  bDebug:=false;
  timState.Enabled:=false;
  timMonitor.Enabled:=false;
  sPlayerName:='ds.exe';
  sAgentName:='dsagn.exe';
  iOnlineInterval:=1;
  iMonInterval:=1;
  iOfflineInterval:=10;
  sOnlineSMPLog:='online_smp';
  sOnlineAGNLog:='online_agn';
  sOnlineMONLog:='online_mon';
  sProductSMP:='InfoExpress SMP';
  sProductAGN:='InfoExpress Agent';
  sPRoductMON:='InfoExpress Monitor';

  sTrackLog:='track_mon';
  urlMonitor:='http://www.revenue-express.com/smp/mon.php';
  urlUpdate:='http://www.revenue-express.com/smp/';
  iniFile:=TiniFile.Create(sProgramDir+'config.ini');
  try
    sPlayerName:=iniFile.ReadString('main','player','ds.exe');
    sAgentName:=iniFile.ReadString('main','agent','dsagn.exe');
    iDelMaxDay:=iniFile.ReadInteger('main','del_max_day',90);
    iOnlineInterval:=iniFile.ReadInteger('online_status','interval',1);
    iOfflineInterval:=iniFile.ReadInteger('online_status','offline',10);
    sOnlineSMPLog:=iniFile.ReadString('online_log','player','online_smp');
    sOnlineAGNLog:=iniFile.ReadString('online_log','agent','online_agn');
    sOnlineMonLog:=iniFile.ReadString('online_log','monitor','online_mon');
    sProductSMP:=iniFile.ReadString('product','player','InfoExpress SMP');
    sProductAGN:=iniFile.ReadString('product','agent','InfoExpress Agent');
    sProductMon:=iniFile.ReadString('product','monitor','InfoExpress Mon');
    bTrack:=(1=iniFile.ReadInteger('track','monitor',0));
    bDebug:=(1=iniFile.ReadInteger('debug','monitor',0));
    iMonInterval:=iniFile.ReadInteger('timer_request','interval',5);
    sTrackLog:=iniFile.ReadString('track_log','monitor','track_mon');
    urlUpdate:=iniFile.ReadString('server','update','localhost');
    urlMonitor:=iniFile.ReadString('server','monitor','localhost');
    urlRegister:=iniFile.ReadString('server','register','localhost');
  finally
    iniFile.Free;
  end;

  timState.Interval:=1000*iOnlineInterval;
  timMonitor.Interval:=1000*iMonInterval;
//  timState.Enabled:=true;
//  sleep(100);
//  timMonitor.Enabled:=true;

  frmLog.lblVersion.Caption:='Version '+cValMonitor;
//    + ','+ txtVerPlayer.Text
//    + ','+ txtVerAgent.Text;

  CreateDefaultFolder();

  if (FileExists(sStarter)) then begin
    sMove:=sProgramDir+'DSStarter.exe';
    DeleteFile(sMove);
    MoveFile(pchar(sStarter),pchar(sMove));
  end;
  RemoveExpiredFile(sProgramDir+'media\',iDelMaxDay);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var iSelButton:integer;
begin
  setDefault();

//  KeyHook.AddHotKey(vk_f11);
//  KeyHook.AddHotKey(vk_f12);

  ShowWindow(Application.Handle, SW_HIDE);
  SetWindowLong(Application.Handle, GWL_EXSTYLE,
          GetWindowLong(Application.Handle, GWL_EXSTYLE) or WS_EX_TOOLWINDOW);

  MemoryStream := TMemoryStream.Create;

  sProgramDir:=ExtractFilePath(Application.ExeName);

//  ExpireCheck;
{
  if (CheckExpire) then begin
    iSelButton:=MessageDlg(cMsgExpire,mtInformation,[mbOK],0);
    GetExpire();
    Application.Terminate;
    abort;
  end;
  SaveTrack('Program Start');
  StartProgram();
}
end;

procedure TfrmMain.timMonitorTimer(Sender: TObject);
var
  sValue:string;
begin
//  KillWindows(PAnsiChar('DS Agent'));
  if (not bInProcess) then
  begin
    timMonitor.Enabled:=false;
    bInProcess:=true;
    try
      checkAgent();
      checkPlayer();
    finally
      sValue:=formatdatetime('yyyy-mm-dd hh:nn:ss',now);
      if (bDebug) then
        frmLog.Memo1.Lines.Add(sValue+'# Monitor Record');
      bInprocess:=false;
      timMonitor.Enabled:=true;
    end;
  end;
end;

function FileInUse(FileName: string): Boolean;
var hFileRes: HFILE;
begin
  Result := False;
  if not FileExists(FileName) then exit;
  hFileRes := CreateFile(PChar(FileName),
                                    GENERIC_READ or GENERIC_WRITE,
                                    0,
                                    nil,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,
                                    0);
  Result := (hFileRes = INVALID_HANDLE_VALUE);
  if not Result then
    CloseHandle(hFileRes);
end;

function KillWindows(const sCapt: PChar) : boolean;
  var AppHandle:THandle;
begin
  AppHandle:=FindWindow(Nil, sCapt) ;
  Result:=false;
  if (AppHandle > 0) then
  Result:=PostMessage(AppHandle, WM_QUIT, 0, 0) ;
end;

function processExists(exeFileName: string): Boolean;
var
  ContinueLoop: Boolean;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  Result := False;
  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
    begin
      Result := True;
    end;
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;

function KillTask(ExeFileName: string): Integer;
const
  PROCESS_TERMINATE = $0001;
var
  ContinueLoop: Boolean;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  Result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
      Result := Integer(TerminateProcess(
                        OpenProcess(PROCESS_TERMINATE,
                                    BOOL(0),
                                    FProcessEntry32.th32ProcessID),
                                    0));
     ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;

procedure TfrmMain.txtProcessChange(Sender: TObject);
begin
  btnKill.Enabled:=false;
  btnFind.Enabled:=false;
  if (length(txtProcess.Text) > 0) then
    btnFind.Enabled:=true;
end;

procedure TfrmMain.btnFindClick(Sender: TObject);
begin
  if processExists(txtProcess.Text+'.exe') then
    begin
      ShowMessage('process is running') ;
      btnKill.Enabled:=true;
    end
  Else
    begin
      ShowMessage('process not running') ;
    end;
end;

procedure TfrmMain.btnKillClick(Sender: TObject);
begin
  if (KillTask(txtProcess.Text+'.exe') >0) then
    begin
      ShowMessage('process is terminate') ;
      btnKill.Enabled:=false;
    end
  Else
    begin
      ShowMessage('process not terminate') ;
    end;
end;


function TfrmMain.checkAlive(sType:string; sRecord: string):boolean;
var
  dtCurrent: TDateTime;
  dtLast: TDateTime;
  Day,Time: TStringList;
  Input: TStringList;
  myLogFile:TextFile;
  iDiff:integer;
  sFile,sValue,sNow : string;
  SR: TSearchRec;
begin
  result:=false;
  sFile:=sProgramDir+'log/'+sRecord+'.log';
//  sFile:=sProgramDir+sRecord;
{
  while (fileinuse(sFile)) do sleep(500);
  if (fileexists(SFile)) then
    begin
    AssignFile(myLogFile,sFile);
    Reset(myLogFile);
    ReadLn(myLogFile, sValue);
    CloseFile(myLogFile);
    end;
}
  Input:=TStringList.Create;
  Day:=TStringList.Create;
  Time:=TStringList.Create;
  if (FileExists(sFile)) then begin
    sValue:=getFileDate(sFile,'modify','yyyy-mm-dd hh:nn:ss');
//    dtLast:=FileTimeToDTime(getFileDate(sFile,'modify')));
    Split(' ',sValue,Input);
    Split('-',Input[0],Day);
    Split(':',Input[1],Time);
    dtLast:=EncodeDateTime(strtoInt(Day[0]),strtoint(Day[1]),strtoint(Day[2]),
      strtoint(Time[0]),strtoint(Time[1]),strtoint(Time[2]),0);
//  if FindFirst(sFile, faAnyFile, SR) = 0 then
//    begin
//    dtLast := FileTimeToDTime(SR.FindData.ftLastWriteTime);
//    dtLast := FileTimeToDTime(SR.FindData.ftLastAccessTime);
    end
  Else
    begin
    sValue:='2013-01-01 00:00:00';
    Split(' ',sValue,Input);
    Split('-',Input[0],Day);
    Split(':',Input[1],Time);
    dtLast:=EncodeDateTime(strtoInt(Day[0]),strtoint(Day[1]),strtoint(Day[2]),
      strtoint(Time[0]),strtoint(Time[1]),strtoint(Time[2]),0);
    end;
  sValue := formatdatetime('yyyy-mm-dd hh:nn:ss',dtLast);
  try
    sNow:=formatdatetime('yyyy-mm-dd hh:nn:ss',now);
    iDiff:=SecondsBetween(Now,dtLast);
    if (iDiff <= iOfflineInterval ) then
      begin
      if (bDebug) then
        frmLog.Memo1.Lines.Add(sNow+'# checkAlive '+sType+' online last access '+inttostr(iDiff)+' seconds. '+sValue);
      result:=true;
      end
    Else
      begin
      if (bDebug) then
        frmLog.Memo1.Lines.Add(sNow+'# checkAlive '+sType+' Offline last access '+inttostr(iDiff)+' seconds. '+sValue);
      end;
  finally
    Day.Free;
    Time.Free;
    Input.Free;
    frmLog.Memo1.Repaint;
  end;
end;

procedure TfrmMain.checkAgent();
var
  sLastStatus: TDateTime;
  Handle:THandle;
  sValue: string;
begin
  try
  sValue:=formatdatetime('yyyy-mm-dd hh:nn:ss',now);
  if (bDebug) then
    frmLog.Memo1.Lines.Add(sValue+'# checkAgent');
  if (processExists(sAgentName)) then
    begin
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent -> Agent Found');
//    if (not checkAlive('Agent',sAgentName)) then
    if (not checkAlive('Agent',sOnlineAgnLog)) then
      begin
        Alert2Server('alert_stop','player');

        if (bDebug) then
          frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent [Offline] -> Kill Player');
        killPlayer();
        if (bDebug) then
          frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent [Offline] -> Kill Agent');
        killAgent();
        if (bDebug) then
          frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent [Offline] -> Start Agent');
        startAgent();
      end
    Else
      begin
      if (bDebug) then
        frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent [Online]');
      end;
    end
  Else
    begin
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent [No Process] -> Kill Player');
    killPlayer();

    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent [No Process] -> Kill Agent');
    killAgent();
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkAgent [No Process] -> Start Agent');
    startAgent();
    end;
  finally
    frmLog.Memo1.Repaint;
  end;
end;

procedure TfrmMain.checkPlayer();
var
  sLastStatus: TDateTime;
  Handle:THandle;
  sValue: string;
begin
  try
  sValue:=formatdatetime('yyyy-mm-dd hh:nn:ss',now);
  if (bDebug) then
    frmLog.Memo1.Lines.Add(sValue+'# checkPlayer');
  if (processExists(sPlayerName)) then
    begin
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# checkPlayer -> Player Found');
//    if (not checkAlive('Player',sPlayerName)) then
    if (not checkAlive('Player',sOnlineSMPLog)) then
      begin
      SaveTrack('checkPlayer [Offline] --> Kill Player');
      if (bDebug) then
        frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# Player [Offline] -> Kill Player');
      killPlayer();
      SaveTrack('checkPlayer [Offline] --> Player Start');
      if (bDebug) then
        frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# Player [Offline] -> Start Player');
      startPlayer();
      end;
    end
  Else
    begin
    SaveTrack('checkPlayer [No Process] --> Kill Player');
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# Player [No Process] -> Kill Player');
    killPlayer();
    SaveTrack('checkPlayer [No Process] --> Start Player');
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# Player [No Process] -> Start Player');
    startPlayer();
    end;
  finally
    frmLog.Memo1.Repaint;
  end;
end;

procedure TfrmMain.chkMonClick(Sender: TObject);
begin
  timMonitor.Enabled:=chkMon.Checked;
end;

procedure TfrmMain.chkStateClick(Sender: TObject);
begin
  timState.Enabled:=chkState.Checked;
end;

procedure TfrmMain.KeyHookHotKeyPressed(HotKey: Cardinal; Index: Word);
begin
  if HotKey = vk_f11 then btnShow.Click;
  if HotKey = vk_f12 then btnHide.Click;
end;

procedure TfrmMain.btnShowClick(Sender: TObject);
var left,right,width,height,posx,posy:integer;
begin
//  posx:=screen.Width-frmLog.Width-10;
//  posy:=screen.Height-frmLog.Height-100;
  posx:=screen.WorkAreaWidth-frmLog.Width;
  posy:=screen.WorkAreaHeight-frmLog.Height;
  if (not frmLog.Showing) then
    begin
    frmLog.Show;
    frmLog.SetFocus;
//    SetWindowPos(Application.Handle,
//               HWND_TOPMOST,
//               0, 0, 0, 0, SWP_NOSIZE);
    end;
  frmLog.Left:=posx;
  frmLog.Top:=posy;
  frmLog.BringToFront;
end;

procedure TfrmMain.btnHideClick(Sender: TObject);
begin
  if frmLog.Showing then frmLog.Hide;
end;

procedure TfrmMain.RxTrayIcon1DblClick(Sender: TObject);
begin
  if frmLog.Showing then begin
    frmMain.btnHide.Click;
  end else begin
    frmMain.btnShow.Click;
  end;
//  if frmLog.Showing then frmLog.Hide;
//  mnuShow.Click;
end;

procedure TfrmMain.btnExitClick(Sender: TObject);
var iSelect: integer;
begin
  iSelect:=MessageDlg('Confirm to Close Application.',mtWarning,mbOKCancel,1);
  if (iSelect = mrOK) then
    begin
    timMonitor.Enabled:=false;
    timState.Enabled:=false;
    killPlayer();
    killAgent();
    frmLog.Close;
    frmMain.Close;
    end;
end;

procedure TfrmMain.mnuExitClick(Sender: TObject);
begin
  btnExit.Click;
end;

procedure TfrmMain.mnuShowClick(Sender: TObject);
begin
  btnShow.Click;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveTrack('Program Close');
  MemoryStream.Free;
end;

procedure TfrmMain.mnuHideClick(Sender: TObject);
begin
  btnHide.Click;
end;

function TfrmMain.GenHTTPRequest(sUrl:string; sCmd: string; sDevice: string): string;
begin
  result := sUrl+'?cmd='+sCmd+'&attr='+sDevice
        + '&mac='+txtMac.Text
        + '&mc='+txtSerial.Text;
//       + '&clientid='+sClientID;
//  if forvideo then
//    result := http_server + video_program + memo1.Lines[cmd]
//                + param1 + '&terminalid=' + inttostr(Terminal_ID)
//                + '&mac=' + edtMAC.Text
//                + '&sys_pwd=' + Client_PWD
//                + '&clientid=' + inttostr(Client_ID)
end;

procedure TfrmMain.Alert2Server(sType:string;sDevice:string);
var
  httpstr: string;
  sLastOnline: string;
  sCurrentTime: string;
  myLogFile: TextFile;
  sFile,shttp,sError:string;
  sCategory:string;
  stmHTTP:TMemoryStream;
begin


  shttp := GenHTTPRequest(urlMonitor,sType,sDevice);
  httpstr:= StringReplace(shttp, ' ', '%20',
                          [rfReplaceAll, rfIgnoreCase]);
  stmHTTP:=TMemoryStream.Create;
  try
    Application.ProcessMessages;
    IdHTTP1.get(httpstr, stmHTTP);
    stmHTTP.Position := 0;
    frmMain.Memo1.Lines.LoadFromStream(stmHTTP);
    Application.ProcessMessages;
  except
    on E : Exception do begin
      sError:='Exception on Function = Alert2Server';
      sError:=sError+#13#10+'http=> "'+httpstr+'"';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
      Application.ProcessMessages;
    end;
  end;
  stmHTTP.Free;
end;

procedure TfrmMain.killPlayer(bLog:boolean=FALSE);
begin
  SaveTrack('Kill Player');
  if (not bLog) then
    frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# killPlayer.');
  if (KillTask(sPlayerName) > 0) then
    begin
    KillWindows(PAnsiChar(sProductSMP));
    Alert2Server('alert_stop','player');
    sleep(100);
    SaveTrack('killPlayer -> Success.');
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# killPlayer -> Success.');
    end
  Else
    begin
    SaveTrack('killPlayer -> Failed.');
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# killPlayer -> Failed.');
    end;
end;

procedure TfrmMain.startPlayer(bLog:boolean=FALSE);
begin
  Sleep(5000);
  SaveTrack('Start Player');
  ShellExecute(Handle, 'open', pchar(sProgramDir+sPlayerName), nil, nil, SW_SHOWNORMAL);
  Alert2Server('alert_start','player');
  if (not bLog) then
    frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# startPlayer');
  if (bDebug) then
    begin
    frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# startPlayer -> Success');
    end;
end;


procedure TfrmMain.killAgent(bLog:boolean=FALSE);
begin
  SaveTrack('Kill Agent');
  if (not bLog) then
    frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# killAgent.');
  if (KillTask(sAgentName)> 0) then
    begin
    KillWindows(PAnsiChar(sProductAGN));
    Alert2Server('alert_stop','agent');
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# killAgent -> Success.');
    SaveTrack('killAgent -> Success.');
    end
  Else
    begin
    SaveTrack('killAgent -> Failed.');
    if (bDebug) then
      frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# killAgent -> Failed.');
    end;
  sleep(500);
end;

procedure TfrmMain.startAgent(bLog:boolean=FALSE);
var sDB: string;
begin
  SaveTrack('Start Agent');
  sDB:=sProgramDir+'dbx.mdb';
  if (FileExists(sDB)) then
    deletefile(sDB);
  ShellExecute(Handle, 'open', pchar(sProgramDir+sAgentName), nil, nil, SW_SHOWNORMAL);
  Alert2Server('alert_start','agent');
  if (not bLog) then
    frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# startAgent');
  if (bDebug) then
    begin
    frmLog.Memo1.Lines.Add(formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# startAgent -> Success');
    end;
end;

procedure TfrmMain.RxTrayIcon1Click(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if frmLog.Showing then begin
    btnHide.Click;
  end else begin
    btnShow.Click
  end;

  //  mnuShow.Click;
end;

procedure TfrmMain.StartProgram();
begin
  killAgent(TRUE);
  checkAgent();
  checkPlayer();
  frmLog.Show;
//  if (frmLog.Showing) then frmLog.Hide;
  timMonitor.Enabled:=true;
  timState.Enabled:=true;
end;

function TfrmMain.CheckExpire():boolean;
var
  sValExpire:string;
  sCurrDate:string;
  sCheck:string;
  iniFile: TINIFILE;
begin
  result:=true;

  iniFile:=TiniFile.Create(sProgramDir+'license.ini');
  try
    sValExpire:=iniFile.ReadString('License','Expired','2000-01-01');
  finally
    iniFile.Free;
  end;
  sCurrDate:=formatdatetime('yyyy-mm-dd',now);
  sCheck:=copy(sValExpire,0,2);
  if (sCheck <> '20') then
    sValExpire:=formatdatetime('yyyy-mm-dd',now+50);
  if (sCurrDate <= sValExpire) then result:=false;

end;

procedure TfrmMain.GetExpire();
var
  httpstr: string;
  sExpire,shttp,sCheck:string;
  iniFile: TINIFILE;
  stmHTTP:TMemoryStream;
begin

  shttp := GenHTTPRequest(urlRegister,'getexpire','monitor');
  httpstr:= StringReplace(shttp, ' ', '%20',
                          [rfReplaceAll, rfIgnoreCase]);
  stmHTTP:=TMemoryStream.Create;;
  try
    Application.ProcessMessages;
    stmHTTP.Clear;
    IdHTTP1.get(httpstr, stmHTTP);
    stmHTTP.Position := 0;
    Memo1.Lines.LoadFromStream(stmHTTP);
    sExpire:=Memo1.Lines[0];
//    dll_addRegistry(cProduct,cKeyExpire,pchar(sExpire));
    Application.ProcessMessages;
  except
    Application.ProcessMessages;
  end;
  stmHTTP.Free;
  sCheck:=copy(sExpire,0,2);
  if (sCheck = '20') then begin

    iniFile:=TiniFile.Create(sProgramDir+'license.ini');
    try
      iniFile.WriteString('License','Expired',sExpire);
    finally
      iniFile.Free;
    end;
  end;

end;

procedure TfrmMain.ExpireCheck();
var iSelButton:integer;
begin
  if (FileExists(sProgramDir+'license.ini')) then begin
    GetExpire();
    if (CheckExpire) then begin
      iSelButton:=MessageDlg(cMsgExpire,mtInformation,[mbOK],0);
      SaveTrack('Program Expired');
//      Application.Terminate;
//    end else begin
    end;
    SaveTrack('Program Start');
    StartProgram();
  //  Self.Hide();
  end else
    Application.Terminate;

end;

procedure TfrmMain.CreateDefaultFolder();
var sLogFile:string;
begin
  // Create Log Folder
  if (not DirectoryExists(sProgramDir+'log\')) then begin
    sLogFile:=sProgramDir+'log\';
    CreateDir(sLogFile);
  end;

  // Create Media Folder
  if (not DirectoryExists(sProgramDir+'media\')) then begin
    sLogFile:=sProgramDir+'media\';
    CreateDir(sLogFile);
  end;

  // Delete Player Log

  sLogFile:=sProgramDir+'log\'+sOnlineSMPLog+'.log';
  if (FileExists(sLogFile)) then DeleteFile(sLogFile);

  // Delete Agent Log
  sLogFile:=sProgramDir+'log\'+sOnlineAGNLog+'.log';
  if (FileExists(sLogFile)) then DeleteFile(sLogFile);
end;

procedure TfrmMain.RemoveExpiredFile(sFolder:string;iDelMaxDay:integer=30);
var
  sMesg,sFile,sAccess,sNow,sName:string;
  iFile,iDiff,iCount:integer;
begin
  mmoFiles.Lines.Clear;
  ListFileDir(sFolder,mmoFiles.Lines);
  sMesg:='Delete file that not use '+inttostr(iDelMaxDay)+' Day(s)';
  iCount:=0;
  for iFile:=0 to mmoFiles.Lines.Count-1 do begin
    sName:=mmoFiles.Lines[iFile];
    if (not ((LeftStr(sName,1)='V') or (LeftStr(sName,1)='B'))) then
      continue;
    sFile:=sFolder+mmoFiles.Lines[iFile];
    sAccess:=GetFileDate(sFile,'access','dd/mm/yyyy hh:nn:ss');
    sNow:=datetimetostr(Now());
    iDiff:=DaysBetween(strtodatetime(sAccess),Now());
    if (iDiff < iDelMaxDay) then continue;

    sMesg:=sMesg+#13#10+' '+mmoFiles.Lines[iFile];
//    sMesg:=sMesg+' create: '+ GetFileDate(sFile,'create');
//    sMesg:=sMesg+' modify: '+ GetFileDate(sFile,'modify');
    sMesg:=sMesg+' last access: '+ GetFileDate(sFile,'access');
//    sMesg:=sMesg+' sAccess: '+ sAccess+ '<> ' + sNow;
    sMesg:=sMesg+' diff: '+ inttostr(iDiff)+ ' Days';
    DeleteFile(sFile);
    inc(iCount);
  end;
  sFileList.Free;
  if (iCount > 1) then
    ShowMessage(sMesg);
end;

end.
