object frmLog: TfrmLog
  Left = 255
  Top = 676
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Monitor Log'
  ClientHeight = 178
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblVersion: TLabel
    Left = 8
    Top = 8
    Width = 85
    Height = 16
    Caption = 'Version 1.0.4'
    Font.Charset = THAI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object txtMac: TEdit
    Left = 8
    Top = 32
    Width = 145
    Height = 24
    Font.Charset = THAI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Fixedsys'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    Text = 'txtMac'
    Visible = False
  end
  object Memo1: TMemo
    Left = 8
    Top = 40
    Width = 353
    Height = 129
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    OnChange = Memo1Change
  end
  object btnClear: TButton
    Left = 368
    Top = 48
    Width = 59
    Height = 25
    Caption = 'Clear Log'
    TabOrder = 1
    OnClick = btnClearClick
  end
  object btnMinimize: TButton
    Left = 368
    Top = 88
    Width = 59
    Height = 25
    Caption = 'Minimize'
    TabOrder = 2
    OnClick = btnMinimizeClick
  end
  object RxClock1: TRxClock
    Left = 288
    Top = 8
    Width = 73
    Height = 25
    Font.Charset = THAI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object txtSerial: TEdit
    Left = 192
    Top = 8
    Width = 81
    Height = 24
    BiDiMode = bdRightToLeft
    Enabled = False
    Font.Charset = THAI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Fixedsys'
    Font.Style = []
    ParentBiDiMode = False
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
  end
  object btnReload: TButton
    Left = 368
    Top = 8
    Width = 59
    Height = 25
    Caption = 'Refresh'
    TabOrder = 6
    OnClick = btnReloadClick
  end
  object btnClose: TButton
    Left = 368
    Top = 128
    Width = 59
    Height = 25
    Caption = 'Close'
    TabOrder = 7
    OnClick = btnCloseClick
  end
  object timExpire: TTimer
    OnTimer = timExpireTimer
    Left = 256
    Top = 8
  end
end
