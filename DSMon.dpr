program DSMon;

uses
  Forms,
  fmMain in 'fmMain.pas' {frmMain},
  fmLog in 'fmLog.pas' {frmLog};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'DS Monitor';
  Application.CreateForm(TfrmLog, frmLog);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
